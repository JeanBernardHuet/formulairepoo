<?php

/**
 * Classe "abstraite" qui ne peut pas être instanciée
 *
 * Cette classe décrit ce que doit être un formulaire, ce qu'il peut faire et comment.
 * Cette classe ne décrit pas la représentation du formulaire, mais bien son fonctionnement.
 *
 * @author Jean-Bernard HUET
 *
 * @version 1.0.0
 */
abstract class TFormManager
{

    /**
     */
    public function __construct()
    {}
}

